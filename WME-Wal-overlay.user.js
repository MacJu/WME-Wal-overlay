// ==UserScript==
// @name         WME Wal overlay
// @namespace    https://www.bowlman.org/
// @grant          GM_info
// @version      2018.08.07.05
// @description  A script to add Wallonia"s cities limits on the Waze map
// @author       romain-dehasseleer,tunisiano187,MacJu89
// @license        MIT/BSD/X11
// @updateURL      https://gitlab.com/WMEScripts/WME-City-overlay/raw/master/WME-City-overlay.user.js
// @compatible     chrome firefox
// @supportURL      mailto:incoming+WMEScripts/WME-City-overlay@incoming.gitlab.com
// @contributionURL https://ko-fi.com/tunisiano
// @grant        none
// @require      http://openlayers.org/api/OpenLayers.js
// @include      https://www.waze.com/*/editor/*
// @include      https://editor-beta.waze.com/*
// @connect      geoservices.wallonie.be
// @match          https://editor-beta.waze.com/*editor*
// @match          https://beta.waze.com/*editor*
// @match          https://www.waze.com/*editor*
// @match          https://editor-beta.waze.com/*editor/*
// @match          https://beta.waze.com/*editor/*
// @match          https://www.waze.com/*/editor/*
// ==/UserScript==


$(window).load(function() {
    ///////////////////////////////////////
    //      Configuration initiale       //
    ///////////////////////////////////////
    var ScriptName = GM_info.script.name;
    var WMEWalOverlayVersion = GM_info.script.version;
    var WMEWalOverlay = {};

    var Lam2WGS = function(x,y) { // Adapted from https://github.com/leny/bl72ToLatLng/blob/master/index.js
        var LongRef = 0.076042943,
            nLamb = 0.7716421928,
            aCarre = Math.pow( 6378388, 2 ),
            bLamb = 6378388 * ( 1 - ( 1 / 297 ) ),
            eCarre = ( aCarre - Math.pow( bLamb, 2 ) ) / aCarre,
            KLamb = 11565915.812935,
            eLamb = Math.sqrt( eCarre ),
            eSur2 = eLamb / 2,
            Tan1 = ( x - 150000.01256 ) / ( 5400088.4378 - y ),
            Lambda = LongRef + ( 1 / nLamb ) * ( 0.000142043 + Math.atan( Tan1 ) ),
            RLamb = Math.sqrt( Math.pow( ( x - 150000.01256 ), 2 ) + Math.pow( ( 5400088.4378 - y ), 2 ) ),
            TanZDemi = Math.pow( ( RLamb / KLamb ), ( 1 / nLamb ) ),
            Lati1 = 2 * Math.atan( TanZDemi ),
            Haut = 0,
            eSin, Mult1, Mult2, Mult, LatiN, Diff, lat, lng,
            Lat, Lng, LatWGS84, LngWGS84, DLat, DLng, Dh, dy, dx, dz, da, df, LWa, Rm, Rn,
            LWb, LWf, LWe2, SinLat, SinLng, CoSinLat, CoSinLng, Adb;

        do {
            eSin = eLamb * Math.sin( Lati1 );
            Mult1 = 1 - eSin;
            Mult2 = 1 + eSin;
            Mult = Math.pow( ( Mult1 / Mult2 ), ( eLamb / 2 ) );
            LatiN = ( Math.PI / 2 ) - ( 2 * ( Math.atan( TanZDemi * Mult ) ) );
            Diff = LatiN - Lati1;
            Lati1 = LatiN;
        } while( Math.abs( Diff ) > 0.0000000277777 );

        lat = ( LatiN * 180 ) / Math.PI;
        lng = ( Lambda * 180 ) / Math.PI;

        Lat = ( Math.PI / 180 ) * lat;
        Lng = ( Math.PI / 180 ) * lng;

        SinLat = Math.sin( Lat );
        SinLng = Math.sin( Lng );
        CoSinLat = Math.cos( Lat );
        CoSinLng = Math.cos( Lng );

        dx = -125.8;
        dy = 79.9;
        dz = -100.5;
        da = -251.0;
        df = -0.000014192702;

        LWf = 1 / 297;
        LWa = 6378388;
        LWb = ( 1 - LWf ) * LWa;
        LWe2 = ( 2 * LWf ) - ( LWf * LWf );
        Adb = 1 / ( 1 - LWf );

        Rn = LWa / Math.sqrt( 1 - LWe2 * SinLat * SinLat );
        Rm = LWa * ( 1 - LWe2 ) / Math.pow( ( 1 - LWe2 * Lat * Lat ), 1.5 );

        DLat = -dx * SinLat * CoSinLng - dy * SinLat * SinLng + dz * CoSinLat;
        DLat = DLat + da * ( Rn * LWe2 * SinLat * CoSinLat ) / LWa;
        DLat = DLat + df * ( Rm * Adb + Rn / Adb ) * SinLat * CoSinLat;
        DLat = DLat / ( Rm + Haut );

        DLng = ( -dx * SinLng + dy * CoSinLng ) / ( ( Rn + Haut ) * CoSinLat );
        Dh = dx * CoSinLat * CoSinLng + dy * CoSinLat * SinLng + dz * SinLat;
        Dh = Dh - da * LWa / Rn + df * Rn * Lat * Lat / Adb;

        LatWGS84 = ( ( Lat + DLat ) * 180 ) / Math.PI;
        LngWGS84 = ( ( Lng + DLng ) * 180 ) / Math.PI;

        console.log(LatWGS84.toFixed( 6 ),LngWGS84.toFixed( 6 )) //vérifier combien de chiffres on doit retourner à WME
        //return {
       //     "latitude": +( LatWGS84.toFixed( 6 ) ),
        //    "longitude": +( LngWGS84.toFixed( 6 ) )
    };

    ///////////////////////////////////////
    //  Verification de la mise à jour   //
    ///////////////////////////////////////
    var WMEWalOverlayUpdateNotes = "Nouvelle version de WMEWalOverlay : " + WMEWalOverlayVersion;
    if (localStorage.getItem('WMEWalOverlayVersion') === WMEWalOverlayVersion && 'WMEWalOverlayVersion' in localStorage) {

    } else if ('WMEWalOverlayVersion' in localStorage) {
        alert(WMEWalOverlayUpdateNotes);
        localStorage.setItem('WMEWalOverlayVersion', WMEWalOverlayVersion);
    } else {
        localStorage.setItem('WMEWalOverlayVersion', WMEWalOverlayVersion);
    }

    ///////////////////////////////////////
    // Création de l'onglet Wall-Overlay //
    ///////////////////////////////////////
    var tab = $('<li><a href="#sidepanel-WMEWal" data-toggle="tab" id="WMEWalTab">' + ScriptName + '</li>');
    $("#user-info ul.nav-tabs").first().append(tab);
    $("#user-tabs ul.nav-tabs").first().append(tab);

    tab = '<div class="tab-pane" id="sidepanel-WMEWal" >';
	$("#user-info div.tab-content").first().append(tab);
	$("#user-tabs div.tab-content").first().append(tab);


    ///////////////////////////////////////////////////////
    // On récupère la position de l'éditeur sur la carte //
    ///////////////////////////////////////////////////////
    var latitude, longitude, zone,townSelected,shapeSelected;
    latitude = W.map.center.lat;
    longitude = W.map.center.lon;

    var url='https://geoservices.wallonie.be/cadmap/rest/getListeCommunes';
    var select = document.createElement("select");
    var communes =$.getJSON(url, function(data){
        var listecommunes=JSON.stringify(data);
        var jsonData = JSON.parse(listecommunes);
        var i;
        select.setAttribute("id", "SelectRegion");
        select.options.add( new Option("---",'') );
        for (i in jsonData) {
            select.options.add( new Option(jsonData[i].nom,jsonData[i].ins) );
        }
        //$(select).on('change', myFunction);
        //select.setAttribute("onchange", 'alert(this.value)');
        $("#sidepanel-WMEWal").append(select);
        $("#SelectRegion").change(WMEWalOverlay.SelectRegionFunction);
    });
    WMEWalOverlay.SelectRegionFunction = function() {
            var selectSections = document.createElement("select");
            var url='https://geoservices.wallonie.be/cadmap/rest/getListeDivisions/' + SelectRegion.value;
            var Sections = $.getJSON(url, function(data){
               var listesections=JSON.stringify(data);
                var jsonData = JSON.parse(listesections);
                var i;
                selectSections.setAttribute("id", "SelectSection");
                selectSections.options.add( new Option("---",'') );
                for (i in jsonData) {
                    selectSections.options.add( new Option(jsonData[i].divNom,jsonData[i].codeDiv) );
                }
                if($("#SelectSection")[0]) {
                    $("#SelectSection").replaceWith(selectSections);
                } else {
                    $("#sidepanel-WMEWal").append(selectSections);
                }
                $("#SelectSection").change(WMEWalOverlay.SelectSectionFunction);
        });
    };
    WMEWalOverlay.SelectSectionFunction = function() {
        var url='https://geoservices.wallonie.be/cadmap/rest/getShapeDivision/' + SelectSection.value;
        var shapes =$.getJSON(url, function(data){
            var coordonnees = JSON.parse(JSON.stringify(data.polygones[0].coordonnees));
            for (var i in coordonnees){
                //console.log(coordonnees[i].x, coordonnees[i].y);

                ////FONCTION CONV///
                Lam2WGS( coordonnees[i].x, coordonnees[i].y );
            };
        });
    };
   /* $.ajax({
        type: "GET",
        dataType: 'jsonp',
        url: url,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            communes = JSON.stringify(data);

            $("#sidepanel-WMEWal").append(communes);
            $("#sidepanel-WMEWal").append('test');
        }});
    //prévoir le cas où les données sont null
    navigator.geolocation.getCurrentPosition(function(location) {
        latitude = location.coords.latitude;
        longitude = location.coords.longitude;
        window.location.href = "https://www.waze.com/fr/editor?lon="+longitude+"&lat="+latitude+"&zoom=5";
        $(document).ready(function(){
            //On effectue des appels ajax pour éviter une erreur cross domain

            //townSelected=$.parseJSON(JSON.stringify(data));
            $.ajax({
                type: "GET",
                dataType: 'jsonp',
                url: "https://geoservices.wallonie.be/cadmap/rest/getShapeDivision/53053",
                async: false,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    shapeSelected=JSON.stringify(data);
                    //{"ins":"53053","lang":"F","nom":"Mons","xMin":113498.258500002,"xMax":129856.244999997,"yMin":118606.868999999,"yMax":133741.347199999},
                    for(var i=0;i<places.length;i++){
                        if(places[i].nom===zone){ //mis par défaut sur Mons, sera remplacé par l'input user

                            townSelected=places[i];
                            var shape =$.getJSON("https://geoservices.wallonie.be/cadmap/rest/getShapeDivision/"+townSelected.ins, function(data){
                                shapeSelected=JSON.stringify(data);

                            });
                            break;

                        }
                    }
                },
                error: function(err,msg){
                    console.log('erreur  shape:'+msg);

                }
            });

        });
        window.onload = function () {
            // var itm=localStorage.getItem("shapeSelected");
            // Dessiner la zone géographique(polygon)
            // get the canvas element using the DOM
            var canvas = document.getElementById('OpenLayers_Layer_Vector_RootContainer_319_svgRoot'); //récupération du containeur de la map
            var ctx = canvas.getContext('2d');
            ctx.fillStyle = '#f00';
            ctx.beginPath();
            ctx.moveTo(0, 0); //dessiner le polygone
            for(var i=1;i<shapeSelected.polygones[0].coordonees.length-1;i++){
                ctx.lineTo(shapeSelected.polygones[0].coordonees[i].x,shapeSelected.polygones[0].coordonees[i].y);
            }
            ctx.closePath();
            ctx.fill();
            console.log('ok');
        }



});*/
});
